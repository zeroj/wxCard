// pages/mycard/mycard.js
//获取应用实例
var app = getApp()
Page({
  data: {
    cardData: {
      shopName: '',
      mainBusiness: '',
      special: '',
      contact: '',
      location: '',
      pictures: []
    }
  },
  onShareAppMessage: function (e) {

  },
  shareToFriend: function (e) {

  },
  onTapCardDetail: function (e) {
    wx.navigateTo({
      url: '../createcard/createcard?id=0',
    });
  },
  getCardData: function () {
    var that = this;
    wx.getStorage({
      key: 'myCardInfo',
      success: function(res) {
        if (res.data != null) {
          that.setData({
            cardData: res.data,
          });
        }
      },
    });
  },
  openCreatecard: function () {
    wx.navigateTo({
      url: '../createcard/createcard?id=0',
    });
  },
  radioChange: function (e) {
  },
  onLoad: function (options) {
    this.getCardData();
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
    this.getCardData();
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})