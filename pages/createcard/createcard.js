// pages/createcard/createcard.js
//获取应用实例
var app = getApp()

Page({
  data: {
    iseng: true,
    ischinese: true,
    isen: false,
    cardData: {
      shopName: '',
      mainBusiness: '',
      special: '',
      contact: '',
      location: '',
      pictures: []
    }
  },
  cardDataShopName: function (e) {
    this.setData({
      'cardData.shopName': e.detail.value
    })
  },
  cardDataMainBusiness: function (e) {
    this.setData({
      'cardData.mainBusiness': e.detail.value
    })
  },
  cardDataSpecial: function (e) {
    this.setData({
      'cardData.special': e.detail.value
    })
  },
  cardDataContact: function (e) {
    this.setData({
      'cardData.contact': e.detail.value
    })
  },
  cardDataLocation: function (e) {
    this.setData({
      'cardData.location': e.detail.value
    })
  },
  cardDataEmail: function (e) {
    this.setData({
      'cardData.data.email': e.detail.value
    })
  },

  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'myCardInfo',
      success: function(res) {
        if (res.data != null) {
          // 编辑
          that.setData({
            cardData: res.data,
          });
          wx.setNavigationBarTitle({
            title: '编辑名片'
          });
        }
        else {
          // 创建
          wx.setNavigationBarTitle({
            title: '创建名片'
          });
        }
      },
    });

  },

  onReady: function () {
    // 页面渲染完成    
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },


  openAddress: function () {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        that.setData({
          'cardData.data.address': res.address,
          'cardData.data.loglat': res.latitude + ',' + res.longitude
        })
        that.setData({
          'enCardData.data.address': res.address,
          'enCardData.data.loglat': res.latitude + ',' + res.longitude
        })
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },

  formSubmit: function (e) {
    var that = this
    if (e.detail.value.shopName === '') {
      wx.showToast({
        title: '店铺名称必填',
        image: '../../images/error.png',
        duration: 2000
      })
    }
    else {
      wx.setStorageSync('myCardInfo', that.data.cardData);
      wx.showToast({
        title: '保存成功',
      })
      wx.navigateBack({
        
      });
    }
  },
})