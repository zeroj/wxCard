// pages/mycard/mycard.js
//获取应用实例
var app = getApp()
Page({
  data: {
    cardData: {
      shopName: '',
      mainBusiness: '',
      special: '',
      contact: '',
      location: '',
      pictures: []
    }
  },

  radioChange: function (e) {
  },
  onLoad: function (options) {
    console.log(options);
    if (options) {
      var collectedCards = app.globalData.collectedCards;
      if (collectedCards.length != 0) {
        var card = collectedCards[options.index];
        console.log(card);
        this.setData({
          cardData: card,
        })
      }

    }

  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})