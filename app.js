//app.js
App({
  onLaunch: function (options) {
    //调用API从本地缓存中获取数据
    var that = this
    console.log(options);

    if (options != null) {
      var id = options.query;
      if (id != null) {
        wx.navigateTo({
          url: `pages/carddetail/carddetail?index=${id}`,
        });
      }
    }
  },
  globalData: {
    collectedCards: [
      {
        shopName: '胖哥烧烤',
        mainBusiness: '海鲜烧烤、特色小菜',
        special: '炭烤螃蟹、无骨鸡爪、烤猪血饼',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '1',
        id: 1
      },
      {
        shopName: '大眼儿麻辣烫',
        mainBusiness: '色麻辣烫、麻辣烫套餐',
        special: '特色牛丸、麻辣白菜、经典鸭血',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '2',
        id: 2

      },
      {
        shopName: '东北饺子馆',
        mainBusiness: '水饺、家常菜、东北经典菜',
        special: '五彩水饺、豆角陷饺子、东北风味',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '3',
        id: 3
      },
      {
        shopName: '胖哥烧烤',
        mainBusiness: '海鲜烧烤、特色小菜',
        special: '炭烤螃蟹、无骨鸡爪、烤猪血饼',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '4',
        id: 4
      },
      {
        shopName: '胖哥烧烤',
        mainBusiness: '海鲜烧烤、特色小菜',
        special: '炭烤螃蟹、无骨鸡爪、烤猪血饼',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '5',
        id: 5

      },
      {
        shopName: '胖哥烧烤',
        mainBusiness: '海鲜烧烤、特色小菜',
        special: '炭烤螃蟹、无骨鸡爪、烤猪血饼',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '6',
        id: 6
      },
      {
        shopName: '胖哥烧烤',
        mainBusiness: '海鲜烧烤、特色小菜',
        special: '炭烤螃蟹、无骨鸡爪、烤猪血饼',
        contact: '15268735624',
        location: '详细地址',
        lastDate: '7',
        id: 7
      },
    ]
  },
  getLogiCallback: function (val, callback) {
    var that = this
    var token = wx.getStorageSync('token')
    if (token) {
      callback()
    } else {
      wx.login({
        success: function (body) {
          if (body.code) {
            wx.request({
              url: "https://api.91ygj.com/vCard/Card/jscode2session?code=" + body.code,
              data: {
              },
              success: function (body) {
                wx.getUserInfo({
                  success: function (res) {
                    wx.setStorageSync('userInfo', res.userInfo)
                    var data = {
                      'iv': res.iv,//wx.getUserInfo接口返回那里的iv
                      'rawData': res.rawData, //wx.getUserInfo接口返回那里的iv
                      'signature': res.signature,// wx.getUserInfo接口返回那里的signature
                      'encryptedData': res.encryptedData,  //wx.getUserInfo接口返回那里的encryptedData
                      'session_key': body.data.session_key //wx.login接口下面 “code 换取 session_key” 获得
                    }

                    that.getLoginData(data, function (res2) {
                      wx.setStorageSync('token', res2.data.token)
                      wx.setStorageSync('loginSuccessData', res2.data)
                      callback(res2.data.token)
                    })
                  },
                  fail: function (res){
                    wx.showModal({
                      title: '警告',
                      showCancel:true,
                      cancelText:'不授权',
                      confirmText:'授权',
                      content: '若不授权微信登陆，则无法使用蚁管家名片功能；点击重新获取授权，则可重新使用；若点击不授权，后期还使用小程序，需在微信【发现】——【小程序】——删掉【蚁管家名片】，重新搜索授权登陆，方可使用。',
                      success: function (res) {
                        if (res.confirm) {
                          wx.openSetting({
                            success:function(res){
                              if (!res.authSetting["scope.userInfo"] || !res.authSetting["scope.userLocation"]) {
                                that.getLogiCallback('',      function(){
                                  callback('')
                                })
                              }
                            }
                          })
                        } else if (res.cancel) {
                        }
                      }
                    }) 
                  }
                })
              }
            })
          } else {
            console.log('登录失败')
          }
        }
      })
    }

  },
  getLoginData: function (data, callback) {
    var url = 'https://api.91ygj.com/vCard/VCardUser/Login'
    wx.request({
      url: url,
      method: 'POST',
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        callback(res.data)
      },
      fail: function (res) {
        console.log('请求出错')
      }
    })
  },
  
  getData: function (data, callback) {
    var url = 'https://api.91ygj.com/vCard/' + data
    var token = wx.getStorageSync('token')
    wx.request({
      url: url,
      data: {},
      header: {
        'token': token,
        'content-type': 'application/json'
      },
      success: function (res) {
        callback(res.data)
      },
      fail: function (res) {
        console.log('请求出错')
      }
    })
  },
  postData: function (data, callback) {
    var url = 'https://api.91ygj.com/vCard/' + data.url
    var token = wx.getStorageSync('token')
    wx.request({
      url: url,
      method: 'POST',
      data: data.data,
      header: {
        'token': token,
        'content-type': 'application/json'
      },
      success: function (res) {
        callback(res.data)
      },
      fail: function (res) {
        console.log('请求出错')
      }
    })
  },
})